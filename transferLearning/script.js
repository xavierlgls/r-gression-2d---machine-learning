let mobileNet, classifier;
let video, chart;
var canvas;
let chartConfig = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Loss',
            data: [],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
};
let currentLearningId = null;
let imagesCount = {};
let lossList = [];

const params = {
    prepareDuration: 1, //s
    learnDuration: 3 //s
}

const options = {
    version: 1,
    alpha: 1.0,
    topk: 3,
    learningRate: 0.0001,
    hiddenUnits: 100,
    epochs: 20,
    numLabels: 2,
    batchSize: 0.4,
};

const model = 'MobileNet'; // 'Darknet', 'Darknet-tiny', 'DoodleNet', 'MobileNet'

function setup() {
    canvas = createCanvas(640, 480);
    canvas.parent('canvas');
    // canvas = createCanvas(1280, 720);
    video = createCapture(VIDEO);
    video.hide();
    mobileNet = ml5.featureExtractor(model, options, onModelLoaded);
    classifier = mobileNet.classification(video, onVideoReady);
}

function draw() {
    image(video, 0, 0, width, height);
    if (currentLearningId != null) {
        imagesCount[currentLearningId]++;
        classifier.addImage('obj' + currentLearningId);
    }
}

function onModelLoaded() {
    popSuccess("Modèle prêt");
}

function onVideoReady() {
    popSuccess("Vidéo prête");
}

function onResult(error, results) {
    if (error) {
        popError(error);
    } else {
        displayResult(results);
        setTimeout(function () { classifier.classify(onResult); }, 200);
    }
}

function displayResult(results) {
    var resultDiv = document.querySelector("#results");
    var label;
    if (results == 'obj1') {
        if (document.querySelector('input[name="obj1"]').value != "") {
            label = document.querySelector('input[name="obj1"]').value;
        } else {
            label = "Objet 1";
        }
    } else if (results == 'obj2') {
        if (document.querySelector('input[name="obj2"]').value != "") {
            label = document.querySelector('input[name="obj2"]').value;
        } else {
            label = "Objet 2";
        }
    }
    resultDiv.innerHTML = '<h3><span class="badge badge-pill badge-warning">Prédiction: <u>' + label + '</u></span></h3>';
}

function getLabels(size, color) {
    output = [];
    index = 0;
    while (index < size) {
        output.push(index + 1);
        index++;
    }
    return output;
}

function initLossDisplay() {
    document.getElementById('chart').style.display = 'block';
    var ctx = document.getElementById('chart').getContext('2d');
    chart = new Chart(ctx, chartConfig);
}

function addLoss(value) {
    lossList.push(value);
    chartConfig.data.datasets[0].data.push(value);
    if (chartConfig.data.labels.length < chartConfig.data.datasets[0].data.length) {
        chartConfig.data.labels.push(chartConfig.data.labels.length);
    }
    chart.update();
}

function beginLearning(id) {
    currentLearningId = id;
    document.querySelector("#btn" + id).classList.remove("btn-warning");
    document.querySelector("#btn" + id).classList.add("btn-success");
    if (!imagesCount[id]) {
        imagesCount[id] = 0;
    }
    setTimeout(stopLearning, params.learnDuration * 1000);
}

function stopLearning() {
    var id = currentLearningId;
    currentLearningId = null;
    document.querySelector("#btn" + id).classList.remove("btn-success");
    document.querySelector("#btn" + id).classList.add("btn-primary");
    popSuccess(imagesCount[id] + " images apprises au total pour l'objet " + id);
}

function whileTraining(loss) {
    if (loss == null) {
        popSuccess("Apprentissage terminé !");
        document.querySelector("#train").classList.add("btn-info");
        classifier.classify(onResult);
    } else {
        addLoss(loss);
    }
}

function onButton1() {
    document.querySelector("#btn1").classList.remove("btn-primary");
    document.querySelector("#btn1").classList.add("btn-warning");
    setTimeout(beginLearning, params.prepareDuration * 1000, 1);
}

function onButton2() {
    document.querySelector("#btn2").classList.remove("btn-primary");
    document.querySelector("#btn2").classList.add("btn-warning");
    setTimeout(beginLearning, params.prepareDuration * 1000, 2);
}

function onButtonTrain() {
    initLossDisplay();
    document.querySelector("#train").classList.remove("btn-info");
    classifier.train(whileTraining);
}