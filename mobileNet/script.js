let mobileNet;
let video;
var canvas;

const options = {
    version: 1,
    alpha: 1.0,
    topk: 3, // nombre de classes affichées
  };

const model = 'MobileNet'; // 'Darknet', 'Darknet-tiny', 'DoodleNet', 'MobileNet'

// fonction de la librairie de rendu graphique p5.js qui est appelée lorsque le DOM est chargé à l'initialisation
function setup() {
    canvas = createCanvas(640, 480);
    canvas.parent('canvas');
    // canvas = createCanvas(1280, 720);
    video = createCapture(VIDEO);
    video.hide();
    mobileNet = ml5.imageClassifier(model, video, options, onModelLoaded);
}

// fonction de la librairie de rendu graphique p5.js qui est appelée à chaque nouvelle frame
function draw() {
    image(video, 0, 0, width, height);// affichage de la dernière image du flux vidéo de la camera
}

function onModelLoaded() {
    popSuccess("Modèle prêt");
    mobileNet.predict(onResult);
}

function onResult(error, results) {
    if (error) {
        popError(error);
    } else {
        displayResult(results);
        setTimeout(function () { mobileNet.predict(onResult); }, 200);// prédiction toutes les 200ms
    }
}

//affichage du résultat de la classification sous forme de barres de progression
function displayResult(results) {
    var resultDiv = document.querySelector("#results");
    resultDiv.innerHTML = "";
    results.forEach(result => {
        let value = Math.round(100 * result.probability);
        let main = document.createElement("div");
        main.style.margin = "10px";
        let container = document.createElement("div");
        container.classList = "progress";
        let bar = document.createElement("div");
        bar.classList = "progress-bar";
        bar.setAttribute("role", "progressbar");
        bar.setAttribute("aria-min", "0");
        bar.setAttribute("aria-max", "100");
        bar.style.width = value + "%";
        bar.innerHTML = result.className;
        container.appendChild(bar);
        main.appendChild(container);
        resultDiv.appendChild(main);
    });
}