let mobileNetX, mobileNetY, predictorX, predictorY;
let video, chart;
// Configuration du graph pour l'affichage de l'évolution de loss au cours des epochs
let chartConfig = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Loss X',
            data: [],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)'
            ],
            borderWidth: 1
        }, {
            label: 'Loss Y',
            data: [],
            backgroundColor: [
                'rgba(66, 170, 245, 0.2)'
            ],
            borderColor: [
                'rgba(66, 170, 245, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
};
var canvas;
let registrationMod = false;
let learning = false;
let displayResult = false;
let step = null;
let currentCursor = { x: null, y: null, count: 0 };
let prediction = { x: null, y: null };
let storedCursors = [];
let imagesCount = {};
let lossListX = [], lossListY = [];

const params = {
    prepareDuration: 3, //s
    learnDuration: 6 //s
}

// =================== Paramétrage du transfer learning ===================
const options = {
    version: 1,
    alpha: 1.0,
    topk: 3,
    learningRate: 0.0001,
    hiddenUnits: 100,
    epochs: 20,
    numLabels: 2,
    batchSize: 0.4,
};

// Nom du modèle importé
const model = 'MobileNet'; // 'Darknet', 'Darknet-tiny', 'DoodleNet', 'MobileNet'

// fonction de la librairie de rendu graphique p5.js qui est appelée lorsque le DOM est chargé à l'initialisation
function setup() {
    canvas = createCanvas(640, 480);
    canvas.parent('canvas');
    // canvas = createCanvas(1280, 720);
    video = createCapture(VIDEO);
    video.hide();
    mobileNetX = ml5.featureExtractor(model, options, onModelLoaded);
    mobileNetY = ml5.featureExtractor(model, options, onModelLoaded);
    predictorX = mobileNetX.regression(video, onVideoReady);
    predictorY = mobileNetY.regression(video, onVideoReady);
}

// fonction de la librairie de rendu graphique p5.js qui est appelée à chaque nouvelle frame
function draw() {
    image(video, 0, 0, width, height);// affichage de la dernière image du flux vidéo de la camera
    // affichage des marqueurs des enregistrements stockés
    storedCursors.forEach(cursor => {
        fill('blue');
        circle(cursor.x, cursor.y, 10);
        text(cursor.count + ' échantillons', cursor.x + 5, cursor.y - 5);
    });
    if (learning) { // Phase d'enregistrement de données
        let color = 'blue';
        if (step == 'prepare') {
            color = 'orange';
        } else if (step == 'learning') {
            color = 'green';
            currentCursor.count++;
            // Ajout de l'image courante avec les positions X et Y respectivement à la base d'apprentissage
            predictorX.addImage(currentCursor.x / width);
            predictorY.addImage(currentCursor.y / height);
        }
        fill(color);
        circle(currentCursor.x, currentCursor.y, 20);
        if (step == 'prepare') {
            text('enregistrement imminent', currentCursor.x + 10, currentCursor.y - 5);
        } else if (step == 'learning') {
            text('enregistrement en cours', currentCursor.x + 10, currentCursor.y - 5);
        }
    } else if (displayResult) { // Phase de prédiction
        fill('yellow');
        circle(Math.round(prediction.x * width), Math.round(prediction.y * height), 20);
    }
}

// fonction de la librairie de rendue p5.js appellée lors d'un clic de souris
function mouseClicked() {
    if (registrationMod && mouseInCanvas() && !learning) {
        startLearning(mouseX, mouseY);
    }
}

function mouseInCanvas() {
    return mouseX >= 0 && mouseY >= 0 && mouseX <= width && mouseY <= height;
}

function onModelLoaded() {
    popSuccess("Modèle prêt");
}

function onVideoReady() {
    popSuccess("Vidéo prête");
}

function onResultX(error, results) {
    if (error) {
        popError(error);
    } else {
        prediction.x = results;
        setTimeout(function () { predictorX.predict(onResultX); }, 200); // prédiction toutes les 200ms
    }
}

function onResultY(error, results) {
    if (error) {
        popError(error);
    } else {
        prediction.y = results;
        setTimeout(function () { predictorY.predict(onResultY); }, 200); // prédiction toutes les 200ms
    }
}

// init du graph
function initLossDisplay() {
    document.getElementById('chart').style.display = 'block';
    var ctx = document.getElementById('chart').getContext('2d');
    chart = new Chart(ctx, chartConfig);
}

// stockage des loss et update du graphique
function addLossX(value) {
    lossListX.push(value);
    chartConfig.data.datasets[0].data.push(value);
    if (chartConfig.data.labels.length < chartConfig.data.datasets[0].data.length) {
        chartConfig.data.labels.push(chartConfig.data.labels.length);
    }
    chart.update();
}

// stockage des loss et update du graphique
function addLossY(value) {
    lossListY.push(value);
    chartConfig.data.datasets[1].data.push(value);
    if (chartConfig.data.labels.length < chartConfig.data.datasets[1].data.length) {
        chartConfig.data.labels.push(chartConfig.data.labels.length);
    }
    chart.update();
}

// marqueur devient orange
function startLearning(x, y) {
    learning = true;
    currentCursor.x = x;
    currentCursor.y = y;
    currentCursor.count = 0;
    step = 'prepare';
    setTimeout(beginLearning, params.prepareDuration * 1000);
}

// marqueur devient vert
function beginLearning() {
    step = 'learning';
    setTimeout(stopLearning, params.learnDuration * 1000);
}

// marqueur devient bleu
function stopLearning() {
    learning = false;
    step = null;
    storedCursors.push({ x: currentCursor.x, y: currentCursor.y, count: currentCursor.count });
}

function whileTrainingX(loss) {
    if (loss == null) {
        popSuccess("X: Apprentissage terminé !");
        predictorX.predict(onResultX);
    } else {
        addLossX(loss);
    }
}

function whileTrainingY(loss) {
    if (loss == null) {
        popSuccess("Y: Apprentissage terminé !");
        predictorY.predict(onResultY);
        displayResult = true;
    } else {
        addLossY(loss);
    }
}

function onRegButton() {
    if (!registrationMod) {
        document.querySelector("#reg").classList.add("btn-primary");
        document.querySelector("#reg").classList.remove("btn-secondary");
        document.querySelector("#reg").textContent = "Mode enregistrement activé";
        registrationMod = true;
    } else {
        document.querySelector("#reg").classList.add("btn-secondary");
        document.querySelector("#reg").classList.remove("btn-primary");
        document.querySelector("#reg").textContent = "Mode enregistrement désactivé";
        registrationMod = false;
    }

}

function onTrainButton() {
    if (storedCursors.length < 2) {
        alert("Attention il n'y a pas assez de données pour l'apprentissage !")
    } else {
        initLossDisplay();
        document.querySelector("#train").classList.remove("btn-info");
        predictorX.train(whileTrainingX);
        predictorY.train(whileTrainingY);
    }
}