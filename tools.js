function popError(msg) {
    popMsg("danger", msg);
}

function popSuccess(msg) {
    popMsg("success", msg);
}

function popMsg(type, msg) {
    var div = document.createElement("div");
    div.classList = "alert alert-" + type + " alert-dismissible fade show";
    div.role = "alert";
    div.innerText = msg;
    var button = document.createElement("button");
    button.classList = "close";
    button.setAttribute("type", "button");
    button.setAttribute("data-dismiss", "alert");
    button.setAttribute("aria-label", "Close");
    var span = document.createElement("span");
    span.innerText = "✕";
    span.setAttribute("aria-hidden", "true");
    button.appendChild(span);
    div.appendChild(button);
    document.querySelector("#alerts").appendChild(div);
}